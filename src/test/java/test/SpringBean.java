package test;

public class SpringBean
{
	public SpringProperty property;
	public int num;
	public SpringProperty getProperty() {
		return property;
	}
	public void setProperty(SpringProperty property) {
		this.property = property;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
}
