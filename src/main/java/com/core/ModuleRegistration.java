package com.core;

import com.core.interfaces.IHandlerManager;
import com.core.interfaces.IMessageManager;

/**
 * 抽象的模块注册类 
 * 感觉写的不好 给点意见
 * @author King
 *
 */
public abstract class ModuleRegistration
{
	/**注册action**/
	public abstract void registerModule();
	
	protected IMessageManager messageManager;
	
	protected IHandlerManager handerManager;
	
	public ModuleRegistration(IMessageManager messageManager,IHandlerManager handerManager)
	{
		this.messageManager = messageManager;
		this.handerManager = handerManager;
	}
	
	
	protected void register(int moduleId,AbsHandlerRegistration handerRegister)
	{
		handerRegister.moduleId = moduleId;
		handerRegister.messageManager = messageManager;
		handerRegister.handerManager = handerManager;
		handerRegister.registerHandler();
	}
}
