package com.core.script;
/**
 * 脚本不能在重载的时候替换包
 * 不能有构造函数 
 * 不能有内部数据  只能有方法
 * 具体实现的方法必须有接口
 * @author King
 *
 */
public interface IScript {
	
	/**获得脚本id**/
	public int getId();
}
