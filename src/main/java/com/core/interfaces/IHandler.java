package com.core.interfaces;

import com.core.initialize.Message;
import com.google.protobuf.GeneratedMessage;

/**
 * 通过此接口把网络层与逻辑层业务逻辑分离
 * @author King
 *
 */
public interface IHandler 
{
	public void handler(Message msg) throws Throwable;
	
	/**
	 * 初始化消息体对象
	 * @return
	 */
	public abstract Class<? extends GeneratedMessage> initBodyClass();
}
