package com.core.interfaces;

import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.MessageLite;

/**
 * 消息体管理器接口
 * @author King
 *
 */
public interface IMessageManager {
	/**
	 * 获取消息体
	 * @param msgCla
	 * @param body
	 * @return
	 */
	public abstract MessageLite getBody(
			Class<? extends GeneratedMessage> msgCla, byte[] body);
	/**
	 * 增加一个消息体对象
	 * @param modelId
	 * @param actionId
	 * @param msgCla
	 */
	public abstract void addMessageCla(int modelId, int actionId,
			Class<? extends GeneratedMessage> msgCla);
	/**
	 * 二进制转对象
	 * @param modelId
	 * @param actionId
	 * @param body
	 * @return
	 */
	public abstract MessageLite getMessage(int modelId, int actionId,
			byte[] body);
	
	/**
	 * 根据 moduleid和actionid获得消息对象
	 * @param modelId
	 * @param actionId
	 * @return
	 */
	public MessageLite getMessage(int modelId, int actionId);
}