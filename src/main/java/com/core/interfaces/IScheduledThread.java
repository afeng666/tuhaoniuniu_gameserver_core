package com.core.interfaces;


/**
 * 计时的单线程接口
 * @author King
 *
 */
public interface IScheduledThread
{
	public abstract void init();

	public abstract void pushAction(Runnable serverAction);

	public abstract void shutDown();

	public abstract String getThreadName();

	/**执行任务**/
	public abstract void exeTask(Runnable command);

}
