package com.core.interfaces;



public interface IMessageExecutorPool
{

	/**
	 * 放入一个信息
	 * @param serverAction
	 */
	public abstract IScheduledThread pushAction(Runnable serverAction);
	

	/**
	 * 关闭
	 * 
	 */
	public abstract void shutDown();

	
	/**
	 * 获取对应线程
	 * 
	 * @param threadName 线程名称
	 *            
	 * @return
	 */
	public IScheduledThread getExecutor(String threadName);
	
	
	/**
	 * 获取随即线程池
	 * @return
	 */
	public IScheduledThread getIndexExecutor();
}