package com.core.rest;

/**
 * 
 * @author macky
 *
 */
public enum RequestMethod {

	GET, PUT, POST, DELETE
}
