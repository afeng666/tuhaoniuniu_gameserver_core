package com.core.rest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;



/**
 * 
 * @author macky
 *
 */
public class RestClient {
	private ArrayList<NameValuePair> params;
	private ArrayList<NameValuePair> headers;
	private String url;
	private int responseCode;
	private String message;
	private String response;

	public String getResponse() {
		return response;
	}

	public String getErrorMessage() {
		return message;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public RestClient(String url) {
		this.url = url;
		params = new ArrayList<NameValuePair>();
		headers = new ArrayList<NameValuePair>();
	}

	public void addParam(String name, String value) {
		params.add(new BasicNameValuePair(name, value));
	}

	public void addHeader(String name, String value) {
		headers.add(new BasicNameValuePair(name, value));
	}

	public void execute(RequestMethod method) throws Exception {
		switch (method) {
		case GET: {
			StringBuffer sBuffer = new StringBuffer();
			if (!params.isEmpty()) {
				sBuffer.append("?");
				for (NameValuePair param : params) {
					if (sBuffer.length() > 1) {
						sBuffer.append("&");
					}
					sBuffer.append(param.getName())
							.append("=")
							.append(URLEncoder.encode(param.getValue(),
									HTTP.UTF_8));
				}
			}
			//HttpGet request = new HttpGet(URI.create(url));
			HttpGet request = new HttpGet(url + sBuffer.toString());
			for (NameValuePair head : headers) {
				request.addHeader(head.getName(), head.getValue());
			}

			executeRequest(request, url);
			break;
		}
		case POST: {
			HttpPost request = new HttpPost(url);
			for (NameValuePair head : headers) {
				request.addHeader(head.getName(), head.getValue());
			}

			if (!params.isEmpty()) {
				request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			}

			executeRequest(request, url);
			break;
		}
		case PUT: {
			HttpPut request = new HttpPut(url);

			for (NameValuePair head : headers) {
				request.addHeader(head.getName(), head.getValue());
			}

			if (!params.isEmpty()) {
				request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			}

			executeRequest(request, url);
			break;
		}
		case DELETE: {
			StringBuffer sBuffer = new StringBuffer();
			if (!params.isEmpty()) {
				sBuffer.append("?");
				for (NameValuePair param : params) {
					if (sBuffer.length() > 1) {
						sBuffer.append("&");
					}
					sBuffer.append(param.getName())
							.append("=")
							.append(URLEncoder.encode(param.getValue(),
									HTTP.UTF_8));
				}
			}

			HttpDelete request = new HttpDelete(url + sBuffer.toString());

			for (NameValuePair head : headers) {
				request.addHeader(head.getName(), head.getValue());
			}

			executeRequest(request, url);
			break;
		}
		}
	}

	private void executeRequest(HttpUriRequest request, String url) {
		HttpClient client = new DefaultHttpClient();
		HttpResponse httpResponse;
		try {
			httpResponse = client.execute(request);
			responseCode = httpResponse.getStatusLine().getStatusCode();
			message = httpResponse.getStatusLine().getReasonPhrase();
			HttpEntity entity = httpResponse.getEntity();
			if (entity != null) {
				InputStream instream = entity.getContent();
				response = convertStreamToString(instream);
				// Closing the input stream will trigger connection release
				instream.close();
			}
		} catch (ClientProtocolException e) {
			client.getConnectionManager().shutdown();
			//Helper.logError(e);
		} catch (IOException e) {
			client.getConnectionManager().shutdown();
			e.printStackTrace();
			//Helper.logError(e);
		}
	}

	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			//Helper.logError(e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				//Helper.logError(e);
			}
		}
		return sb.toString();
	}
}
