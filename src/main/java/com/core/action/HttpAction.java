package com.core.action;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;

import com.core.rest.AbstractHttpHandler;

/**
 * http
 * @author King
 *
 */
public class HttpAction extends BaseAction
{

	private AbstractHttpHandler handler;
	private HttpContent msg;
	//private HttpContent msgContent;
	private ChannelHandlerContext ctx;
	private HttpMethod method;
	private HttpRequest request;
	
	public HttpAction(AbstractHttpHandler handler,Object msg, ChannelHandlerContext ctx, HttpMethod method, HttpRequest request)
	{
		this.handler = handler;
		
		//DefaultLastHttpContent lastHttpContent = (DefaultLastHttpContent)msg;
		this.msg = ((HttpContent)msg).copy();
		
		this.ctx = ctx;
		this.method = method;
		this.request = request;
	}
	
	
	
	@Override
	public void execute() throws Throwable
	{
		handler.process(msg, ctx,method,request);

	}

}
