package com.core.action;

import org.apache.log4j.Logger;

import com.core.initialize.Message;
import com.core.interfaces.IHandler;

/**
 * 收到信息处理action
 * @author King
 *
 */
public class MessageAction extends BaseAction
{
	private static final Logger log = Logger.getLogger(MessageAction.class);
	private IHandler handler;
	private Message msg;
	
	public MessageAction(IHandler handler,Message msg)
	{
		this.handler = handler;
		this.msg = msg;
	}
	
	
	@Override
	public void execute() throws Throwable
	{
		long start = System.nanoTime();
		handler.handler(msg);
		log.warn("收到信息:"+handler.getClass().getSimpleName());
		double userTime = (System.nanoTime() - start)/1000.0/1000.0;
		if (userTime > 0.5) {
			log.error("handler超时:"+userTime+" className: "+handler.getClass().getName());
		}
	}


	public Message getMsg() {
		return msg;
	}
}
