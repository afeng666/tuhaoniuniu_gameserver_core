package com.core.action;

import org.apache.log4j.Logger;

/**
 * 此类方便封装消息队列。
 * @author King
 *
 */
public abstract class BaseAction implements Runnable
{
	private static final Logger log = Logger.getLogger(BaseAction.class);

	public BaseAction()
	{

	}

	public void run()
	{
		try
		{
			execute();
		} catch (Throwable e) {
			BaseAction.log.error("消息执行错误", e);
		}
	}
	
	public abstract void execute() throws Throwable;
}
