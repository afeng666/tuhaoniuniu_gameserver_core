package com.core.async;

import com.core.action.TimeAction;
import com.core.interfaces.IMessageExecutorPool;

/**
 * 异步服务实例
 * 用于从一个线程中丢出某个任务让另外一个线程执行 执行完后丢回给原线程继续去执行
 * 可用于插入数据库等耗时操作
 * @author King
 *
 */
public interface IAsyncService
{

	public abstract void init(IMessageExecutorPool mainExecutor,int asyncPoolSize);

	/**
	 * 增加一个异步回调任务
	 * @param target
	 * @param method
	 * @param params
	 */
	public abstract void addAsyncBackWork(AsyncObject target, String method,
			Object... params);

	/**
	 * 增加一个异步计时任务
	 * @param action
	 */
	public abstract void addAsyncTimeWork(TimeAction action);

	/**
	 * 增加一个异步任务
	 * @param target
	 * @param method
	 * @param params
	 */
	public abstract void addAsyncWork(Object target, String method,
			Object... params);

	/**
	 * 增加一个异步任务
	 * @param target
	 * @param method
	 * @param params
	 */
	public abstract void addAsyncWork(Class target, String method,
			Object... params);
	
	/**关闭**/
	public void shutDown();

}