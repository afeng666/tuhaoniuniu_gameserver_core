package com.core.initialize;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * 客户端用的
 * @author King
 *
 */
public class ClientChannelInitializer extends AbsChannelInitializer
{

	@Override
	protected void initChannel(SocketChannel ch) throws Exception
	{
		ChannelPipeline pipeline = ch.pipeline();
//		//tcp丢包断包处理
		pipeline.addLast("frameDecoder",new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4,0, 0));
		BytesToMessageDecode decode = new BytesToMessageDecode();
		decode.setMessageManager(this.service.getMessageManager());
		pipeline.addLast("byteToMyMessageDecoder", decode);
		//encode
		pipeline.addLast("myMessageToByteEncoder",new MssageToBytesEncode());
		
		ClientChannelHandler handler = new ClientChannelHandler();
		handler.setEnginService(this.service);
		pipeline.addLast("liangShanHandler",handler);
	}

}
