package com.core.initialize;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

import java.util.Map;

import com.core.rest.AbstractHttpHandler;

/**
 * 服务器http初始化
 * @author King
 *
 */
public class ServerHttpInitializer extends AbsChannelInitializer
{

    private Map<String,AbstractHttpHandler>httpHandlers;
    
    public ServerHttpInitializer(Map<String,AbstractHttpHandler>httpHandlers){
        
        this.httpHandlers = httpHandlers;
    }
    
	@Override
	protected void initChannel(SocketChannel ch) throws Exception
	{
		  ChannelPipeline p = ch.pipeline();
	        // Uncomment the following line if you want HTTPS
//	        SSLEngine engine = SecureChatSslContextFactory.getServerContext().createSSLEngine();
//	        engine.setUseClientMode(false);
//          p.addLast("ssl", new SslHandler(engine));
		  	p.addLast("codec", new HttpServerCodec());
	        ServerHttpHandler handler =  new ServerHttpHandler(httpHandlers);
	        handler.setEnginService(this.service);
	        p.addLast("handler", handler);
	}

}
