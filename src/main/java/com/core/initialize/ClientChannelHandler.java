package com.core.initialize;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import com.core.EnginService;
import com.core.testBean.TestMessage.Test;

/**
 * 服务器网络层业务接受handler
 * 由此像业务层派发业务
 * @author King
 *
 */
public class ClientChannelHandler extends SimpleChannelInboundHandler<Message> {

	protected EnginService service;

	public void setEnginService(EnginService service)
	{
		this.service = service;
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);
		Message msge = Message.newMessage();
		msge.setModuleAction(1, 2);
		Test.Builder builder= Test.newBuilder();
		builder.setA(11);
		builder.setB(55);
		builder.setC("嘎嘎");
		msge.setBody(builder);
		ctx.channel().writeAndFlush(msge);
		service.getMessageManager().addMessageCla(10001, 2, Test.class);
	}
	
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Message msg)
			throws Exception 
	{
		System.out.println(msg.getModule()+"  "+msg.getAction());
		Test server = msg.getBody();
		System.out.println(server.getC());
		System.out.println(server.getA());
		
//		IHandler handler = service.getHandlerManager().getHandler(msg.getModule(), msg.getAction());
//		service.getExecutor().pushAction(new MessageAction(handler, msg));
	}
}
