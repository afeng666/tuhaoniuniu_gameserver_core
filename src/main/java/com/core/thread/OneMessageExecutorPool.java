package com.core.thread;

import com.core.interfaces.IScheduledThread;


/**
 * 单线程池
 * @author King
 *
 */
public class OneMessageExecutorPool extends BaseMessageExecutorPool
{
	
	public OneMessageExecutorPool()
	{
		addThreadPoolExecutor();
	}
	
	
	
	@Override
	public IScheduledThread pushAction(Runnable serverAction)
	{
		IScheduledThread thread =   this.threadList.get(0);
		thread.pushAction(serverAction);
		return thread;
	}

}
