package com.core;

import java.util.HashMap;
import java.util.Map;

import com.core.interfaces.IHandler;
import com.core.interfaces.IHandlerManager;



/**
 * 业务处理管理类
 * @author jinmiao
 *
 */
public class HandlerManager implements IHandlerManager
{

	private final Map<Integer, Map<Integer, IHandler>> handlerMap = new HashMap<Integer, Map<Integer, IHandler>>();

	
	/**
	 * 增加一个业务处理类
	 * @param modelId
	 * @param actionId
	 * @param handler
	 */
	public void addHandler(int modelId, int actionId,IHandler handler)
	{
		Map<Integer, IHandler> handlers = handlerMap.get(modelId);
		if(handlers==null)
			handlers = new HashMap<Integer, IHandler>();
		handlers.put(actionId, handler);
		handlerMap.put(modelId, handlers);
	}
	
	/**
	 * 获取一个业务处理类
	 * @param modelId
	 * @param actionId
	 * @return
	 */
	public IHandler getHandler(int modelId, int actionId)
	{
		final Map<Integer, IHandler> handlers = handlerMap.get(modelId);
		if(handlers==null)
		{
//			logger.error("modelId:"+modelId+"actionId:"+actionId+" not found");
			return null;
		}
		return handlers.get(actionId);
	}
}
