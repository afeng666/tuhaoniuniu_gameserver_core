package com.core.config;


import java.util.ArrayList;
import java.util.List;

/**
 * 配置加载
 * @author  jinmiao
 *
 */
public abstract class AbsConfigLoader implements IConfigLoader 
{
	private List<ILoader> loadList = new ArrayList<ILoader>();
	
	/* (non-Javadoc)
	 * @see core.IConfigLoader#load()
	 */
	public abstract void load();
	
	
	protected void loadConfig(ILoader loader)
	{
		loadList.add(loader);
	}
	
	/* (non-Javadoc)
	 * @see core.IConfigLoader#starLoad()
	 */
	public void starLoad()
	{
		load();
		for(ILoader load:loadList)
		{
			load.load();
		}
	}
	
	/* (non-Javadoc)
	 * @see core.IConfigLoader#reLoad()
	 */
	public void reLoad()
	{
		for(ILoader load:loadList)
		{
			load.load();
		}
	}
}
