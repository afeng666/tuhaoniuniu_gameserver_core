package com.core;

import org.springframework.beans.factory.BeanFactory;

import com.core.async.AsyncServiceImpl;
import com.core.async.IAsyncService;
import com.core.config.IConfigLoader;
import com.core.interfaces.IHandlerManager;
import com.core.interfaces.IMessageExecutorPool;
import com.core.interfaces.IMessageManager;
import com.core.script.ScriptManager;

public class EnginService
{
	/**业务线程池**/
	protected IMessageExecutorPool executor;
	/**消息体管理类 获得消息体用**/
	protected IMessageManager messageManager = new MessageManager();
	/**业务处理类管理类**/
	protected IHandlerManager handlerManager = new HandlerManager();
	/**异步线程回调服务**/
	protected IAsyncService asyncService = new AsyncServiceImpl();
	/**channel管理类**/
	protected ChannelManager channelManager;
	
	protected BeanFactory beanFacotry;
	/**配置加载**/
	protected IConfigLoader configLoader;
	
	/**脚本管理器**/
	protected ScriptManager scriptM;

	public IMessageManager getMessageManager() {
		return messageManager;
	}


	public IHandlerManager getHandlerManager() {
		return handlerManager;
	}


	public IMessageExecutorPool getExecutor() {
		return executor;
	}


	public IAsyncService getAsyncService() {
		return asyncService;
	}


	public BeanFactory getBeanFacotry() {
		return beanFacotry;
	}
	
	public ChannelManager getChannelManager() {
		return channelManager;
	}

	public void setChannelManager(ChannelManager channelManager) {
		this.channelManager = channelManager;
	}

	public void shutdown()
	{
		this.executor.shutDown();
		this.asyncService.shutDown();
	}


	public IConfigLoader getConfigLoader() {
		return configLoader;
	}


	public ScriptManager getScriptM() {
		return scriptM;
	}
	
}
