package com.core;

import com.core.interfaces.IHandler;
import com.core.interfaces.IHandlerManager;
import com.core.interfaces.IMessageManager;
import com.google.protobuf.GeneratedMessage;
/**
 * 抽象handler注册类
 * @author King
 *
 */
public abstract class AbsHandlerRegistration
{
	/**注册handler**/
	public abstract void registerHandler();
	
	protected int moduleId;
	
	protected IMessageManager messageManager;
	
	protected IHandlerManager handerManager;
	

	protected void register(int actionId,IHandler handler)
	{
		this.handerManager.addHandler(moduleId, actionId, handler);
		Class<? extends GeneratedMessage> message = handler.initBodyClass();
		if(message!=null)
		{
			this.messageManager.addMessageCla(moduleId, actionId, message);
		}
	}
}
